NAME = aliens
HTML_DIR = site/b
FILE_DIR = site/download
PLASTEX_PATH = ~/philosophy/plastex.themes

all: clean prepare pdfs html htmlonepage odt plaintext epub mobi fb2 remove-tmp

prepare:
	mkdir -p $(FILE_DIR)
	mkdir -p $(FILE_DIR)/html
	mkdir -p $(HTML_DIR)

pdfs: pdf

pdf:
	echo 'CREATE pdf' | tee -a log
	rubber --pdf $(NAME) >> log 2>&1
	mv -f $(NAME).pdf $(FILE_DIR)

html:
	echo 'CREATE HTML' | tee -a log
	XHTMLTEMPLATES=$(PLASTEX_PATH) plastex -d $(HTML_DIR) --theme=myplain --toc-depth=5 --split-level=5 $(NAME).tex >> log 2>&1
	find $(HTML_DIR) -name '*.html' -exec sed -i 's/<div style="width:0.0pt" class="minipage">/<div class="minipage">/g' {} \;
	find $(HTML_DIR) -name '*.html' -exec sed -i 's/flushleft//g' {} \;
	find $(HTML_DIR) -name '*.html' -exec sed -i 's/flushright//g' {} \;
	find $(HTML_DIR) -name '*.html' -exec sed -i 's/<li>\s*<\/li>//g' {} \;
	find $(HTML_DIR) -name '*.html' -exec sed -i 's/>\s*<\/a>/>тут<\/a>/g' {} \;
	rm -f $(HTML_DIR)/*.py;rm $(HTML_DIR)/*.hhc;rm $(HTML_DIR)/*.hhk;rm $(HTML_DIR)/*.hhp;rm $(HTML_DIR)/*.xml;rm $(HTML_DIR)/*.jhm;rm $(HTML_DIR)/*.hs

htmlonepage: prepare
	echo 'CREATE one page HTML' | tee -a log
	htlatex $(NAME).tex "xhtml,charset=utf-8" " -cunihtf -utf8" >> log 2>&1
	cp -f $(NAME).html $(FILE_DIR)/html
	cp -f *.css $(FILE_DIR)/html

odt:
	echo 'CREATE ODT' | tee -a log
	mk4ht oolatex $(NAME).tex >> log 2>&1
	mv -f $(NAME).odt $(FILE_DIR)

plaintext: htmlonepage
	echo 'CREATE plain text' | tee -a log
	elinks -dump -no-references $(NAME).html > $(FILE_DIR)/$(NAME).txt

epub: htmlonepage
	echo 'CREATE ePub' | tee -a log
	ebook-convert $(NAME).html ./$(FILE_DIR)/$(NAME).epub --language ru --no-default-epub-cover >> log 2>&1

mobi: htmlonepage
	echo 'CREATE mobi' | tee -a log
	ebook-convert $(NAME).html ./$(FILE_DIR)/$(NAME).mobi --language ru >> log 2>&1

fb2: htmlonepage
	echo 'CREATE fb2' | tee -a log
	ebook-convert $(NAME).html ./$(FILE_DIR)/$(NAME).fb2 --language ru >> log 2>&1

define removetmp
rm -f $(NAME).css
rm -f $(NAME).html
endef

remove-tmp:
	echo 'REMOVE TMP FILES' | tee -a log
	$(removetmp)

clean:
	echo 'CLEAN' | tee -a log
	$(removetmp)
	rubber --clean $(NAME)
#	rubber --clean $(NAME)-slides
	rm -rf $(FILE_DIR)/*
	rm -rf $(HTML_DIR)/*
	rm -f site.txt
	rm -f log
	rm -f $(NAME).txt
	rm -f *.4ct;rm -f *.aux;rm -f *.idv;rm -f *.log;rm -f *.out;rm -f *.toc;rm -f *.4tc;rm -f *.dvi;rm -f *.lg;rm -f *.msglog;rm -f *.paux;rm -f *.tmp;rm -f *.xref

.SILENT:
